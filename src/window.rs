use servo::{
    compositing::windowing::{AnimationState, EmbedderCoordinates, WindowMethods},
    embedder_traits::Cursor,
    euclid::{Scale, Size2D},
    servo_config::opts,
    style_traits::DevicePixel,
    webrender_api::units::{DeviceIntPoint, DeviceIntRect},
    webrender_traits::RenderingContext,
};
use std::cell::Cell;
use surfman::{Connection, SurfaceType};
use winit::{
    dpi::PhysicalSize,
    event_loop::ActiveEventLoop,
    raw_window_handle::{HasDisplayHandle, HasWindowHandle},
    window::CursorIcon,
};

use crate::{winit_position_to_euclid_point, winit_size_to_euclid_size};

pub struct Window {
    winit_window: winit::window::Window,
    rendering_context: RenderingContext,
    screen_size: Size2D<u32, DevicePixel>,
    inner_size: Cell<Size2D<u32, DevicePixel>>,
    animation_state: Cell<AnimationState>,
}

impl Window {
    pub fn new(event_loop: &ActiveEventLoop) -> Self {
        let opts = opts::get();

        let win_size = opts.initial_window_size.to_untyped().to_i32();
        let width = win_size.width;
        let height = win_size.height;

        let window_attributes = winit::window::Window::default_attributes();
        let winit_window = event_loop
            .create_window(window_attributes)
            .expect("Failed to create window.");

        let primary_monitor = event_loop
            .available_monitors()
            .nth(0)
            .expect("No monitor detected");

        let screen_size = winit_size_to_euclid_size(primary_monitor.size());
        let inner_size = winit_size_to_euclid_size(winit_window.inner_size());

        // Initialize surfman
        let display_handle = winit_window
            .display_handle()
            .expect("could not get display handle from window");
        let connection =
            Connection::from_display_handle(display_handle).expect("Failed to create connection");
        let adapter = connection
            .create_adapter()
            .expect("Failed to create adapter");
        let window_handle = winit_window
            .window_handle()
            .expect("could not get window handle from window");
        let native_widget = connection
            .create_native_widget_from_window_handle(window_handle, Size2D::new(width, height))
            .expect("Failed to create native widget");
        let surface_type = SurfaceType::Widget { native_widget };
        let rendering_context = RenderingContext::create(&connection, &adapter, surface_type)
            .expect("Failed to create WR surfman");

        Self {
            winit_window,
            rendering_context,
            animation_state: Cell::new(AnimationState::Idle),
            inner_size: Cell::new(inner_size),
            screen_size,
        }
    }

    pub fn resize(&self, size: PhysicalSize<u32>) {
        let (width, height) = size.into();
        let new_size = Size2D::new(width, height);
        if self.inner_size.get() != new_size {
            let physical_size = Size2D::new(size.width, size.height);
            self.rendering_context
                .resize(physical_size.to_i32())
                .expect("Failed to resize");
            self.inner_size.set(new_size);
        }
    }

    pub fn request_redraw(&self) {
        self.winit_window.request_redraw();
    }

    pub fn set_title(&self, title: &str) {
        self.winit_window.set_title(title);
    }

    pub fn hidpi_factor(&self) -> f64 {
        self.winit_window.scale_factor()
    }

    pub fn set_cursor(&self, cursor: Cursor) {
        let winit_cursor = match cursor {
            Cursor::Default => CursorIcon::Default,
            Cursor::Pointer => CursorIcon::Pointer,
            Cursor::ContextMenu => CursorIcon::ContextMenu,
            Cursor::Help => CursorIcon::Help,
            Cursor::Progress => CursorIcon::Progress,
            Cursor::Wait => CursorIcon::Wait,
            Cursor::Cell => CursorIcon::Cell,
            Cursor::Crosshair => CursorIcon::Crosshair,
            Cursor::Text => CursorIcon::Text,
            Cursor::VerticalText => CursorIcon::VerticalText,
            Cursor::Alias => CursorIcon::Alias,
            Cursor::Copy => CursorIcon::Copy,
            Cursor::Move => CursorIcon::Move,
            Cursor::NoDrop => CursorIcon::NoDrop,
            Cursor::NotAllowed => CursorIcon::NotAllowed,
            Cursor::Grab => CursorIcon::Grab,
            Cursor::Grabbing => CursorIcon::Grabbing,
            Cursor::EResize => CursorIcon::EResize,
            Cursor::NResize => CursorIcon::NResize,
            Cursor::NeResize => CursorIcon::NeResize,
            Cursor::NwResize => CursorIcon::NwResize,
            Cursor::SResize => CursorIcon::SResize,
            Cursor::SeResize => CursorIcon::SeResize,
            Cursor::SwResize => CursorIcon::SwResize,
            Cursor::WResize => CursorIcon::WResize,
            Cursor::EwResize => CursorIcon::EwResize,
            Cursor::NsResize => CursorIcon::NsResize,
            Cursor::NeswResize => CursorIcon::NeswResize,
            Cursor::NwseResize => CursorIcon::NwseResize,
            Cursor::ColResize => CursorIcon::ColResize,
            Cursor::RowResize => CursorIcon::RowResize,
            Cursor::AllScroll => CursorIcon::AllScroll,
            Cursor::ZoomIn => CursorIcon::ZoomIn,
            Cursor::ZoomOut => CursorIcon::ZoomOut,
            Cursor::None => {
                self.winit_window.set_cursor_visible(false);
                return;
            }
        };
        self.winit_window.set_cursor(winit_cursor);
        self.winit_window.set_cursor_visible(true);
    }

    pub fn is_animating(&self) -> bool {
        self.animation_state.get() == AnimationState::Animating
    }

    pub fn winit_window(&self) -> &winit::window::Window {
        &self.winit_window
    }
}

impl WindowMethods for Window {
    fn get_coordinates(&self) -> EmbedderCoordinates {
        let viewport = DeviceIntRect::from_origin_and_size(
            DeviceIntPoint::zero(),
            winit_size_to_euclid_size(self.winit_window.inner_size()).to_i32(),
        );
        let screen = self.screen_size.to_i32();

        EmbedderCoordinates {
            viewport,
            framebuffer: viewport.size(),
            window: (
                winit_size_to_euclid_size(self.winit_window.outer_size()).to_i32(),
                winit_position_to_euclid_point(
                    self.winit_window.outer_position().unwrap_or_default(),
                ),
            ),
            screen,
            // FIXME: Winit doesn't have API for available size. Fallback to screen size
            screen_avail: screen,
            hidpi_factor: Scale::new(self.hidpi_factor().min(f32::MAX.into()) as f32),
        }
    }

    fn set_animation_state(&self, state: AnimationState) {
        self.animation_state.set(state);
    }

    fn rendering_context(&self) -> RenderingContext {
        self.rendering_context.clone()
    }
}
