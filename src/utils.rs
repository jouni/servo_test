use servo::{
    euclid::{Point2D, Size2D},
    style_traits::DevicePixel,
};
use winit::dpi::{PhysicalPosition, PhysicalSize};

pub fn winit_size_to_euclid_size<T>(size: PhysicalSize<T>) -> Size2D<T, DevicePixel> {
    Size2D::new(size.width, size.height)
}

pub fn winit_position_to_euclid_point<T>(position: PhysicalPosition<T>) -> Point2D<T, DevicePixel> {
    Point2D::new(position.x, position.y)
}

pub fn winit_mouse_to_servo_mouse(
    wbutton: winit::event::MouseButton,
) -> Result<servo::script_traits::MouseButton, ()> {
    let sbutton = match wbutton {
        winit::event::MouseButton::Left => servo::script_traits::MouseButton::Left,
        winit::event::MouseButton::Right => servo::script_traits::MouseButton::Right,
        winit::event::MouseButton::Middle => servo::script_traits::MouseButton::Middle,
        _ => return Err(()),
    };
    Ok(sbutton)
}
