use egui::{CentralPanel, Frame, PaintCallback, Pos2, TopBottomPanel, Vec2};
use egui_glow::{CallbackFn, EguiGlow};
use glow::NativeFramebuffer;
use servo::{
    base::id::TopLevelBrowsingContextId as WebViewId,
    compositing::windowing::EmbedderEvent,
    euclid::{Box2D, Point2D, Scale, Size2D},
    gl,
    servo_geometry::DeviceIndependentPixel,
    style_traits::DevicePixel,
    webrender_api::units::DeviceRect,
    webrender_traits::RenderingContext,
};
use std::{num::NonZeroU32, sync::Arc};
use surfman::GLApi;
use winit::event_loop::ActiveEventLoop;

pub struct Browser {
    widget_surface_fbo: Option<glow::NativeFramebuffer>,
    egui_glow: EguiGlow,
    shapes: Vec<egui::epaint::ClippedShape>,
    textures_delta: egui::TexturesDelta,
    location: Option<String>,
    rect: DeviceRect,
}

impl Browser {
    pub fn new(event_loop: &ActiveEventLoop, rendering_context: &RenderingContext) -> Self {
        let webrender_gl = match rendering_context.connection().gl_api() {
            GLApi::GL => unsafe { gl::GlFns::load_with(|s| rendering_context.get_proc_address(s)) },
            GLApi::GLES => unsafe {
                gl::GlesFns::load_with(|s| rendering_context.get_proc_address(s))
            },
        };
        if webrender_gl.get_error() != gl::NO_ERROR
            || rendering_context.make_gl_context_current().is_err()
        {
            panic!("gl error");
        }

        let gl = unsafe {
            glow::Context::from_loader_function(|s| rendering_context.get_proc_address(s))
        };

        let egui_glow = egui_glow::EguiGlow::new(&event_loop, Arc::new(gl), None, None);

        let widget_surface_fbo = rendering_context
            .context_surface_info()
            .ok()
            .and_then(|x| x.and_then(|x| NonZeroU32::new(x.framebuffer_object)))
            .map(NativeFramebuffer);

        Self {
            widget_surface_fbo,
            egui_glow,
            shapes: vec![],
            textures_delta: Default::default(),
            location: None,
            rect: DeviceRect::default(),
        }
    }

    pub fn set_location(&mut self, x: Option<String>) {
        self.location = x;
    }

    pub fn paint(&mut self, window: &winit::window::Window) {
        unsafe {
            use glow::HasContext as _;
            self.egui_glow
                .painter
                .gl()
                .bind_framebuffer(gl::FRAMEBUFFER, self.widget_surface_fbo);
        }

        let mut textures_delta = self.textures_delta.to_owned();
        for (id, image_delta) in textures_delta.set {
            self.egui_glow.painter.set_texture(id, &image_delta);
        }

        let pixels_per_point = self.egui_glow.egui_ctx.pixels_per_point();
        let clipped_primitives = self
            .egui_glow
            .egui_ctx
            .tessellate(self.shapes.to_owned(), pixels_per_point);
        self.egui_glow.painter.paint_primitives(
            window.inner_size().into(),
            pixels_per_point,
            &clipped_primitives,
        );

        for id in textures_delta.free.drain(..) {
            self.egui_glow.painter.free_texture(id);
        }
    }

    pub fn update(
        &mut self,
        window: &winit::window::Window,
        focused_webview: Option<WebViewId>,
        servo_framebuffer_id: Option<gl::GLuint>,
    ) -> Vec<EmbedderEvent> {
        let mut events = vec![];

        let raw_input = self.egui_glow.egui_winit.take_egui_input(window);
        let widget_fbo = self.widget_surface_fbo;
        let label = self.location.to_owned().unwrap_or("...".into());

        let full_output = self.egui_glow.egui_ctx.run(raw_input, |ctx| {
            TopBottomPanel::bottom("locbar").show(ctx, |ui| {
                ui.allocate_ui_with_layout(
                    ui.available_size(),
                    egui::Layout::left_to_right(egui::Align::Center),
                    |ui| {
                        ui.label(label);
                    },
                );
            });

            let scale =
                Scale::<_, DeviceIndependentPixel, DevicePixel>::new(ctx.pixels_per_point());

            CentralPanel::default()
                .frame(Frame::none())
                .show(ctx, |ui| {
                    let Pos2 { x, y } = ui.cursor().min;
                    let Vec2 {
                        x: width,
                        y: height,
                    } = ui.available_size();
                    let rect =
                        Box2D::from_origin_and_size(Point2D::new(x, y), Size2D::new(width, height))
                            * scale;
                    if rect != self.rect {
                        self.rect = rect;
                        if let Some(x) = focused_webview {
                            events.push(EmbedderEvent::MoveResizeWebView(x, rect));
                        }
                    }
                    let min = ui.cursor().min;
                    let size = ui.available_size();
                    let rect = egui::Rect::from_min_size(min, size);
                    ui.allocate_space(size);

                    let servo_fbo = servo_framebuffer_id
                        .and_then(NonZeroU32::new)
                        .map(NativeFramebuffer);

                    ui.painter().add(PaintCallback {
                        rect,
                        callback: Arc::new(CallbackFn::new(move |info, painter| {
                            let clip = info.viewport_in_pixels();
                            let x = clip.left_px;
                            let y = clip.from_bottom_px;
                            let width = clip.width_px;
                            let height = clip.height_px;

                            unsafe {
                                use glow::HasContext as _;
                                painter.gl().clear_color(0.0, 0.0, 0.0, 0.0);
                                painter.gl().scissor(x, y, width, height);
                                painter.gl().enable(gl::SCISSOR_TEST);
                                painter.gl().clear(gl::COLOR_BUFFER_BIT);
                                painter.gl().disable(gl::SCISSOR_TEST);
                                painter
                                    .gl()
                                    .bind_framebuffer(gl::READ_FRAMEBUFFER, servo_fbo);
                                painter
                                    .gl()
                                    .bind_framebuffer(gl::DRAW_FRAMEBUFFER, widget_fbo);
                                painter.gl().blit_framebuffer(
                                    x,
                                    y,
                                    x + width,
                                    y + height,
                                    x,
                                    y,
                                    x + width,
                                    y + height,
                                    gl::COLOR_BUFFER_BIT,
                                    gl::NEAREST,
                                );
                                painter.gl().bind_framebuffer(gl::FRAMEBUFFER, widget_fbo);
                            }
                        })),
                    });
                });
        });

        self.egui_glow
            .egui_winit
            .handle_platform_output(window, full_output.platform_output);
        self.shapes = full_output.shapes;
        self.textures_delta.append(full_output.textures_delta);

        events
    }
}
