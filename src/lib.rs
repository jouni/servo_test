use serde::Deserialize;
use serde_inline_default::serde_inline_default;
use std::fs;

macro_rules! def {
    ($t:ident) => {
        impl Default for $t {
            fn default() -> Self {
                toml::from_str("").unwrap()
            }
        }
    };
}

#[serde_inline_default]
#[derive(Debug, Deserialize)]
pub struct Keys {}
def!(Keys);

#[serde_inline_default]
#[derive(Debug, Deserialize)]
pub struct Config {
    #[serde(default)]
    pub keys: Keys,
}
def!(Config);

impl Config {
    pub fn new(xdg: &xdg::BaseDirectories) -> Self {
        xdg.find_config_file("config.toml")
            .and_then(|x| fs::read_to_string(x).ok())
            .as_ref()
            .and_then(|x| toml::from_str(x).ok())
            .unwrap_or_default()
    }
}
