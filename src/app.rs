use servo::{
    base::id::TopLevelBrowsingContextId as WebViewId,
    compositing::windowing::{EmbedderEvent, MouseWindowEvent, WindowMethods},
    compositing::CompositeTarget,
    embedder_traits::{CompositorEventVariant, EmbedderMsg, EventLoopWaker},
    euclid::Point2D,
    script_traits::{MouseButton, TraversalDirection},
    servo_url::ServoUrl,
    style_traits::DevicePixel,
    Servo,
};
use std::{cell::Cell, rc::Rc};
use winit::{
    application::ApplicationHandler,
    event::{ElementState, StartCause, WindowEvent},
    event_loop::{ActiveEventLoop, ControlFlow},
    window::WindowId,
};

use crate::{
    browser::Browser, embedder::EmbedderCallbacks, window::Window, winit_mouse_to_servo_mouse,
    winit_position_to_euclid_point, ServoEvent,
};

type Frame = (Rc<Window>, Servo<Window>, Browser);
type ButtonPress = (MouseButton, Point2D<f64, DevicePixel>);

pub struct App {
    frame: Option<Frame>,
    ev_waker: Box<dyn EventLoopWaker>,
    webviews: Vec<WebViewId>,
    focused_webview: Option<WebViewId>,
    current_url: Option<ServoUrl>,
    mouse_pos: Cell<Point2D<f64, DevicePixel>>,
    mouse_down: Cell<Option<ButtonPress>>,
    event_queue: Vec<EmbedderEvent>,
}

impl App {
    pub fn new(ev_waker: Box<dyn EventLoopWaker>) -> Self {
        Self {
            frame: None,
            webviews: vec![],
            ev_waker,
            focused_webview: None,
            current_url: None,
            mouse_pos: Cell::new(Point2D::default()),
            mouse_down: Cell::new(None),
            event_queue: vec![],
        }
    }

    fn handle_events(&mut self) -> bool {
        let (window, servo, browser) = match self.frame.as_mut() {
            Some((w, s, b)) => (w, s, b),
            None => return false,
        };

        let mut present = false;

        {
            let mut servo_events = servo.get_events();

            loop {
                for (webview_id, msg) in servo_events {
                    trace!("servo event: view: {:?} msg: {:?}", webview_id, msg);

                    match msg {
                        EmbedderMsg::ReadyToPresent(_) => window.request_redraw(),
                        EmbedderMsg::WebViewOpened(new_webview_id) => {
                            let rect = window.get_coordinates().get_viewport().to_f32();
                            self.webviews.push(new_webview_id);
                            self.event_queue.extend_from_slice(&[
                                EmbedderEvent::FocusWebView(new_webview_id),
                                EmbedderEvent::MoveResizeWebView(new_webview_id, rect),
                                EmbedderEvent::RaiseWebViewToTop(new_webview_id, true),
                            ]);
                        }
                        EmbedderMsg::WebViewFocused(webview_id) => {
                            self.focused_webview = Some(webview_id);
                            self.event_queue
                                .push(EmbedderEvent::ShowWebView(webview_id, true));
                        }
                        EmbedderMsg::LoadStart
                        | EmbedderMsg::LoadComplete
                        | EmbedderMsg::HeadParsed => present = true,
                        EmbedderMsg::HistoryChanged(urls, current) => {
                            self.current_url = Some(urls[current].clone());
                            browser.set_location(self.current_url.as_ref().map(|x| x.to_string()));
                            present = true;
                        }
                        EmbedderMsg::ChangePageTitle(title) => window.set_title(
                            &title
                                .or(self.current_url.as_ref().map(|x| x.to_string()))
                                .unwrap_or("Servo".into()),
                        ),
                        EmbedderMsg::SetCursor(cursor) => window.set_cursor(cursor),
                        EmbedderMsg::EventDelivered(event) => {
                            if let (Some(wvid), CompositorEventVariant::MouseButtonEvent) =
                                (webview_id, event)
                            {
                                self.event_queue.extend_from_slice(&[
                                    EmbedderEvent::RaiseWebViewToTop(wvid, true),
                                    EmbedderEvent::FocusWebView(wvid),
                                ]);
                            }
                        }
                        EmbedderMsg::WebViewClosed(webview_id) => {
                            self.webviews.retain(|&x| x != webview_id);
                            self.focused_webview = None;
                            self.event_queue.push(
                                self.webviews
                                    .last()
                                    .map(|&x| EmbedderEvent::FocusWebView(x))
                                    .unwrap_or(EmbedderEvent::Quit),
                            );
                        }
                        EmbedderMsg::Shutdown => {
                            return true;
                        }
                        EmbedderMsg::AllowNavigationRequest(pipeline_id, _url) => {
                            if webview_id.is_some() {
                                self.event_queue
                                    .push(EmbedderEvent::AllowNavigationResponse(
                                        pipeline_id,
                                        true,
                                    ));
                            }
                        }
                        _ => {}
                    }
                }

                present |= servo.handle_events(self.event_queue.drain(..));
                servo_events = servo.get_events();

                if servo_events.len() == 0 {
                    break;
                }
            }
        }

        if present {
            servo.present();
            let w = window.winit_window();
            self.event_queue.extend(browser.update(
                w,
                self.focused_webview,
                servo.offscreen_framebuffer_id(),
            ));
            browser.paint(w);
        }

        false
    }
}

impl ApplicationHandler<ServoEvent> for App {
    fn resumed(&mut self, _event_loop: &ActiveEventLoop) {}

    fn window_event(
        &mut self,
        event_loop: &ActiveEventLoop,
        _window_id: WindowId,
        event: WindowEvent,
    ) {
        trace!("window event: {:?}", event);

        let (window, servo, browser) = match self.frame.as_mut() {
            Some((w, s, b)) => (w, s, b),
            None => return,
        };

        match event {
            WindowEvent::RedrawRequested => {
                servo.present();
                let w = window.winit_window();
                self.event_queue.extend(browser.update(
                    w,
                    self.focused_webview,
                    servo.offscreen_framebuffer_id(),
                ));
                browser.paint(w);
                return;
            }
            WindowEvent::Resized(size) => {
                window.resize(size);
                self.event_queue.push(EmbedderEvent::WindowResize);
            }
            WindowEvent::CursorMoved {
                device_id: _,
                position,
            } => {
                let position = winit_position_to_euclid_point(position);
                self.mouse_pos.set(position);
                self.event_queue
                    .push(EmbedderEvent::MouseWindowMoveEventClass(position.to_f32()));
            }
            WindowEvent::MouseInput {
                device_id: _,
                state,
                button,
            } => match button {
                winit::event::MouseButton::Back => {
                    if let Some(x) = self.focused_webview {
                        self.event_queue
                            .push(EmbedderEvent::Navigation(x, TraversalDirection::Back(1)));
                    }
                }
                winit::event::MouseButton::Forward => {
                    if let Some(x) = self.focused_webview {
                        self.event_queue
                            .push(EmbedderEvent::Navigation(x, TraversalDirection::Forward(1)));
                    }
                }
                _ => {
                    if let Ok(sbutton) = winit_mouse_to_servo_mouse(button) {
                        let position = self.mouse_pos.get();
                        let pos = position.to_f32();

                        match state {
                            ElementState::Pressed => {
                                self.mouse_down.set(Some((sbutton, position)));
                                self.event_queue.push(EmbedderEvent::MouseWindowEventClass(
                                    MouseWindowEvent::MouseDown(sbutton, pos),
                                ));
                            }
                            ElementState::Released => {
                                self.event_queue.push(EmbedderEvent::MouseWindowEventClass(
                                    MouseWindowEvent::MouseUp(sbutton, pos),
                                ));
                                if let Some((dbut, dpos)) = self.mouse_down.get() {
                                    let pixel_dist = dpos - position;
                                    let pixel_dist = (pixel_dist.x * pixel_dist.x
                                        + pixel_dist.y * pixel_dist.y)
                                        .sqrt();
                                    if pixel_dist < 10.0 * window.hidpi_factor() {
                                        self.event_queue.push(
                                            EmbedderEvent::MouseWindowEventClass(
                                                MouseWindowEvent::Click(sbutton, pos),
                                            ),
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            },
            WindowEvent::CloseRequested => self.event_queue.push(EmbedderEvent::Quit),
            _ => {}
        }

        if self.handle_events() {
            event_loop.exit();
        }
    }

    fn new_events(&mut self, event_loop: &ActiveEventLoop, cause: StartCause) {
        trace!("start event: {:?}", cause);

        if cause == StartCause::Init {
            let window = Rc::new(Window::new(event_loop));
            let browser = Browser::new(event_loop, &window.rendering_context());

            let embedder = Box::new(EmbedderCallbacks::new(self.ev_waker.clone()));

            let servo_data = Servo::new(embedder, window.clone(), None, CompositeTarget::Fbo);
            let mut servo = servo_data.servo;

            servo.handle_events([EmbedderEvent::NewWebView(
                ServoUrl::parse("https://browserleaks.com").unwrap(),
                servo_data.browser_id,
            )]);

            self.frame = Some((window, servo, browser));

            debug!("init done");
        }

        let (window, servo, browser) = match self.frame.as_mut() {
            Some((w, s, b)) => (w, s, b),
            None => return,
        };

        event_loop.set_control_flow(if window.is_animating() {
            ControlFlow::Poll
        } else {
            ControlFlow::Wait
        });
    }

    fn user_event(&mut self, event_loop: &ActiveEventLoop, _event: ServoEvent) {
        if self.handle_events() {
            event_loop.exit();
        }
    }

    fn exiting(&mut self, _event_loop: &ActiveEventLoop) {
        if let Some(x) = self.frame.take() {
            x.1.deinit();
        }
    }
}
