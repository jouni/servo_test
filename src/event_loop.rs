use servo::embedder_traits::EventLoopWaker;
use std::sync::{Arc, Mutex};
use winit::event_loop::EventLoop;

#[derive(Debug)]
pub struct ServoEvent;

#[derive(Clone)]
pub struct ServoEventLoopWaker(Arc<Mutex<winit::event_loop::EventLoopProxy<ServoEvent>>>);

impl ServoEventLoopWaker {
    pub fn new(event_loop: &EventLoop<ServoEvent>) -> Self {
        Self(Arc::new(Mutex::new(event_loop.create_proxy())))
    }
}

impl EventLoopWaker for ServoEventLoopWaker {
    fn wake(&self) {
        // Kick the OS event loop awake.
        if let Err(err) = self.0.lock().unwrap().send_event(ServoEvent) {
            println!("Failed to wake up event loop ({}).", err);
        }
    }

    fn clone_box(&self) -> Box<dyn EventLoopWaker> {
        Box::new(ServoEventLoopWaker(self.0.clone()))
    }
}
