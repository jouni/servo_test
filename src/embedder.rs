use servo::compositing::windowing::EmbedderMethods;
use servo::embedder_traits::EventLoopWaker;

pub struct EmbedderCallbacks {
    event_loop_waker: Box<dyn EventLoopWaker>,
}

impl EmbedderCallbacks {
    pub fn new(event_loop_waker: Box<dyn EventLoopWaker>) -> EmbedderCallbacks {
        EmbedderCallbacks { event_loop_waker }
    }
}

impl EmbedderMethods for EmbedderCallbacks {
    fn create_event_loop_waker(&mut self) -> Box<dyn EventLoopWaker> {
        self.event_loop_waker.clone()
    }
}
