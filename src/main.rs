#[macro_use]
extern crate log;

mod app;
mod browser;
mod embedder;
mod event_loop;
mod utils;
mod window;

use crate::{
    app::App,
    event_loop::{ServoEvent, ServoEventLoopWaker},
    utils::*,
};

use servo_test::Config;
use std::sync::OnceLock;
use winit::{error::EventLoopError, event_loop::EventLoop};

static CONFIG: OnceLock<Config> = OnceLock::new();

fn main() -> Result<(), EventLoopError> {
    env_logger::init();

    xdg::BaseDirectories::with_prefix("servo_test")
        .ok()
        .as_ref()
        .map(Config::new)
        .and_then(|x| CONFIG.set(x).ok())
        .expect("Failed to initialize config");

    let event_loop = EventLoop::<ServoEvent>::with_user_event().build().unwrap();
    let ev_waker = ServoEventLoopWaker::new(&event_loop);
    let mut app = App::new(Box::new(ev_waker));
    event_loop.run_app(&mut app)
}
